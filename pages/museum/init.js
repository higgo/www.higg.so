   var loaderSymbols = ['0', '1', '2', '3', '4', '5', '6', '7'],
       loaderRate = 100,
       loaderIndex = 0,
       loader = function () {
           document.getElementById('loader').innerHTML = loaderSymbols[loaderIndex];
           loaderIndex = loaderIndex < loaderSymbols.length - 1 ? loaderIndex + 1 : 0;
           setTimeout(loader, loaderRate);
       };

function getTxtFile(file){

       $('pre').load(file, function (response, status, xhr) {
           if (status == 'success') {
               $('.overlay').toggle();
               $('#loader').toggle();
           }
       })

}