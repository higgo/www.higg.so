----------
## The Homepage of David Higgins

### My "put that on version control" website.

Author: David Higgins

Website: [http://davidhiggins.me/](http://davidhiggins.me/)

Last Updated: Thursday, March 08, 2012

You are browsing the Github home of **The Homepage of David Higgins**

This repo contains many different scripts, and projects I have worked on. I like the idea of pooling everything into one repo.


----------